package googlecompute

import (
	"context"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha1"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"strings"
	"time"

	"gitlab.com/jobd/fleeting/fleeting/provider"
	"google.golang.org/api/compute/v1"
)

func (g *InstanceGroup) winrm(ctx context.Context, info *provider.ConnectInfo, instance *compute.Instance) error {
	priv, err := rsa.GenerateKey(rand.Reader, 4096)
	if err != nil {
		return fmt.Errorf("generating private key: %w", err)
	}
	info.Key = nil

	addMetadataKeyValueList(
		instance.Metadata,
		"windows-keys",
		fmt.Sprintf(
			`{"ExpireOn":"%s", "Exponent":"%s", "Modulus":"%s", "UserName":"%s"}`,
			time.Now().Add(5*time.Minute).Format(time.RFC3339),
			base64.StdEncoding.EncodeToString([]byte{byte(priv.E >> 24), byte(priv.E >> 16), byte(priv.E >> 8), byte(priv.E)}),
			base64.StdEncoding.EncodeToString(priv.N.Bytes()),
			info.Username,
		),
	)

	g.log.Info("updating instance metadata", "id", instance.Name)

	// update metadata
	op, err := g.service.Instances.SetMetadata(g.Project, g.Zone, instance.Name, instance.Metadata).Context(ctx).Do()
	if err != nil {
		return err
	}
	if err := g.wait(ctx, op); err != nil {
		return err
	}

	for i := 0; i < 60; i++ {
		info.Password, err = g.getWindowsPasswordFromSerial(ctx, instance.Name, priv)
		if err != nil {
			if ctx.Err() != nil {
				break
			}
			time.Sleep(time.Second)
			continue
		}
		break
	}
	if err != nil {
		return err
	}

	return nil
}

func (g *InstanceGroup) getWindowsPasswordFromSerial(ctx context.Context, id string, priv *rsa.PrivateKey) (string, error) {
	output, err := g.service.Instances.GetSerialPortOutput(g.Project, g.Zone, id).Port(4).Do()
	if err != nil {
		return "", fmt.Errorf("serial port output: %w", err)
	}

	var creds struct {
		ErrorMessage      string `json:"errorMessage,omitempty"`
		EncryptedPassword string `json:"encryptedPassword,omitempty"`
		Modulus           string `json:"modulus,omitempty"`
	}

	for _, line := range strings.Split(output.Contents, "\n") {
		if err := json.Unmarshal([]byte(line), &creds); err != nil {
			continue
		}
		if creds.Modulus == base64.StdEncoding.EncodeToString(priv.N.Bytes()) {
			if creds.ErrorMessage != "" {
				return "", fmt.Errorf("windows credentials agent: %s", creds.ErrorMessage)
			}
			break
		}
	}

	if creds.EncryptedPassword == "" {
		return "", fmt.Errorf("unable to fetch encrypted password from windows credentials agent")
	}

	bp, err := base64.StdEncoding.DecodeString(creds.EncryptedPassword)
	if err != nil {
		return "", fmt.Errorf("decoding windows credentials agent password: %w", err)
	}

	pwd, err := rsa.DecryptOAEP(sha1.New(), rand.Reader, priv, bp, nil)
	if err != nil {
		return "", fmt.Errorf("decrypting windows credentials agent password: %w", err)
	}

	return string(pwd), nil
}
