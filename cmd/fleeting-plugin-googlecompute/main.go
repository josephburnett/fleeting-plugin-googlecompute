package main

import (
	googlecompute "gitlab.com/jobd/fleeting/fleeting-plugin-googlecompute"
	"gitlab.com/jobd/fleeting/fleeting/plugin"
)

func main() {
	plugin.Serve(&googlecompute.InstanceGroup{})
}
