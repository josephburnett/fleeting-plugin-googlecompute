package googlecompute

import (
	"context"
	"crypto/rand"
	"encoding/hex"
	"fmt"
	"io/ioutil"
	"path"
	"time"

	"github.com/hashicorp/go-hclog"
	"gitlab.com/jobd/fleeting/fleeting/provider"
	"google.golang.org/api/compute/v1"
	"google.golang.org/api/option"
)

var _ provider.InstanceGroup = (*InstanceGroup)(nil)

type InstanceGroup struct {
	CredentialsFile string `json:"credentials_file"`
	Project         string `json:"project"`
	Zone            string `json:"zone"`
	Name            string `json:"name"`

	log     hclog.Logger
	service *compute.Service

	settings provider.Settings
}

func (g *InstanceGroup) Init(ctx context.Context, log hclog.Logger, settings provider.Settings) (provider.ProviderInfo, error) {
	g.log = log.With("project", g.Project, "zone", g.Zone, "name", g.Name)
	g.settings = settings

	var options []option.ClientOption
	if g.CredentialsFile != "" {
		contents, err := ioutil.ReadFile(g.CredentialsFile)
		if err != nil {
			return provider.ProviderInfo{}, err
		}

		options = append(options, option.WithCredentialsJSON([]byte(contents)))
	}

	var err error
	g.service, err = compute.NewService(ctx, options...)
	if err != nil {
		return provider.ProviderInfo{}, fmt.Errorf("creating service for project %q, zone %q: %w", g.Project, g.Zone, err)
	}

	return provider.ProviderInfo{
		ID:      path.Join("gce", g.Project, g.Zone, g.Name),
		MaxSize: 1000,
	}, nil
}

func (g *InstanceGroup) Update(ctx context.Context, update func(id string, state provider.State)) error {
	list, err := g.service.InstanceGroupManagers.ListManagedInstances(g.Project, g.Zone, g.Name).Context(ctx).Do()
	if err != nil {
		return err
	}

	for _, instance := range list.ManagedInstances {
		var state provider.State
		switch instance.InstanceStatus {
		case "PROVISIONING", "REPAIRING", "STAGING":
			state = provider.StateCreating
		case "DEPROVISIONING", "STOPPED", "STOPPING", "SUSPENDED", "SUSPENDING", "TERMINATED":
			state = provider.StateDeleting
		case "RUNNING":
			state = provider.StateRunning
		default:
			state = provider.StateCreating
		}

		update(instance.Instance, state)
	}

	return nil
}

func (g *InstanceGroup) Increase(ctx context.Context, delta int) (int, error) {
	instances := make([]*compute.PerInstanceConfig, 0, delta)
	for i := 0; i < delta; i++ {
		var b [8]byte
		_, err := rand.Read(b[:])
		if err != nil {
			return 0, fmt.Errorf("creating random instance name: %w", err)
		}

		instances = append(instances, &compute.PerInstanceConfig{Name: g.Name + "-" + hex.EncodeToString(b[:])})
	}

	req := &compute.InstanceGroupManagersCreateInstancesRequest{Instances: instances}
	op, err := g.service.InstanceGroupManagers.CreateInstances(g.Project, g.Zone, g.Name, req).Context(ctx).Do()
	if err != nil {
		return 0, err
	}

	if err := g.wait(ctx, op); err != nil {
		return 0, err
	}

	return delta, nil
}

func (g *InstanceGroup) Decrease(ctx context.Context, instances []string) ([]string, error) {
	req := &compute.InstanceGroupManagersDeleteInstancesRequest{Instances: instances}

	op, err := g.service.InstanceGroupManagers.DeleteInstances(g.Project, g.Zone, g.Name, req).Context(ctx).Do()
	if err != nil {
		return nil, err
	}

	if err := g.wait(ctx, op); err != nil {
		return nil, err
	}

	return instances, nil
}

func (g *InstanceGroup) wait(ctx context.Context, op *compute.Operation) error {
	now := time.Now()

	for {
		wait, err := g.service.ZoneOperations.Wait(g.Project, g.Zone, op.Name).Context(ctx).Do()
		if err != nil {
			g.log.Error("waiting for operation", "err", err, "op", op.Name, "target", op.TargetLink, "waited", time.Since(now))
		} else {
			g.log.Info("waiting for operation", "op", op.Name, "status", wait.Status, "error", wait.Error, "waited", time.Since(now))
			if wait.Status == "DONE" {
				return nil
			}
		}

		if time.Since(now) > 2*time.Minute {
			return fmt.Errorf("timeout waiting for operation %q, %q in %q, %q", op.Name, op.TargetLink, g.Project, g.Zone)
		}
		time.Sleep(time.Second)
	}
}

func (g *InstanceGroup) ConnectInfo(ctx context.Context, id string) (provider.ConnectInfo, error) {
	id = path.Base(id)
	g.log.Info("fetching instance details", "id", id)

	instance, err := g.service.Instances.Get(g.Project, g.Zone, id).Context(ctx).Do()
	if err != nil {
		return provider.ConnectInfo{}, err
	}

	if instance.Status != "RUNNING" {
		return provider.ConnectInfo{}, fmt.Errorf("instance status is not running (%s)", instance.Status)
	}

	info := provider.ConnectInfo{ConnectorConfig: g.settings.ConnectorConfig}
	g.populateOSArch(&info, instance)
	g.populateNetwork(&info, instance)

	if info.UseStaticCredentials {
		return info, nil
	}

	if info.Protocol == "" {
		info.Protocol = provider.ProtocolSSH
		if info.OS == "windows" {
			info.Protocol = provider.ProtocolWinRM
		}
	}

	switch info.Protocol {
	case provider.ProtocolSSH:
		err = g.ssh(ctx, &info, instance)

	case provider.ProtocolWinRM:
		err = g.winrm(ctx, &info, instance)
	}
	if err != nil {
		return provider.ConnectInfo{}, err
	}

	return info, nil
}

func (g *InstanceGroup) populateOSArch(info *provider.ConnectInfo, instance *compute.Instance) {
	if info.OS == "" {
		info.OS = "linux"
		for _, disk := range instance.Disks {
			if !disk.Boot {
				continue
			}

			for _, feature := range disk.GuestOsFeatures {
				if feature.Type == "WINDOWS" {
					info.OS = "windows"
					break
				}
			}
		}
	}

	if info.Arch == "" {
		info.Arch = "amd64"
	}
}

func (g *InstanceGroup) populateNetwork(info *provider.ConnectInfo, instance *compute.Instance) {
	nic := instance.NetworkInterfaces[0]
	info.InternalAddr = nic.NetworkIP
	if len(nic.AccessConfigs) > 0 {
		info.ExternalAddr = nic.AccessConfigs[0].NatIP
	}
}

func addMetadataKeyValueList(metadata *compute.Metadata, key string, value string) {
	for _, item := range metadata.Items {
		if item.Key == key {
			*item.Value += "\n" + value
			return
		}
	}

	metadata.Items = append(metadata.Items, &compute.MetadataItems{Key: key, Value: &value})
}
